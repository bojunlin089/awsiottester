﻿using AWSIoTTester.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DispenserSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow mMainWindow = null;
        public MainWindow()
        {
            mMainWindow = this;

            UpdateService.SetupUpdateService();
            ConnectService.SetupConnectService();
            DispenserRunService.SetupDispenseService();

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateService.StartUpdateService();
            ConnectService.StartConnectService();
            DispenserRunService.StartDispenseService();
        }

        private void Btn_runDispenser_Click(object sender, RoutedEventArgs e)
        {
            DispenserRunService.IsRun = true;
        }
    }
}
