﻿using AWSIoTTester.Communication;
using AWSIoTTester.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static AWSIoTTester.Main.Topics;
using static DispenserSimulator.MainWindow;
using static AWSIoTTester.Communication.AWSPublisher;

namespace AWSIoTTester.Service
{
    class DispenserRunService
    {
        public static bool IsRun = false;
        private static BackgroundWorker dispenseRoutine = new BackgroundWorker();            //Camera preview
        private static int steps = 0;
        private static DispenserCommand cmd;
        private static CloudData doneJob;

        public static void SetupDispenseService()
        {
            dispenseRoutine.DoWork += new DoWorkEventHandler(dispenseRoutine_doWork);
            dispenseRoutine.ProgressChanged += new ProgressChangedEventHandler(dispenseRoutine_ProgressChanged);
            dispenseRoutine.RunWorkerCompleted += new RunWorkerCompletedEventHandler(dispenseRoutine_WorkerCompleted);
            dispenseRoutine.WorkerReportsProgress = true;
            dispenseRoutine.WorkerSupportsCancellation = true;
        }
        public static void StartDispenseService()
        {
            if (!dispenseRoutine.IsBusy)
                dispenseRoutine.RunWorkerAsync();
        }

        public static void StopDispenseService()
        {
            if (dispenseRoutine.IsBusy)
                dispenseRoutine.CancelAsync();
        }

        private static void dispenseRoutine_doWork(object sender, DoWorkEventArgs e)
        {
            while (!dispenseRoutine.CancellationPending)
            {
                switch (steps)
                {
                    case 0:
                        if (IsRun)
                        {
                            IsRun = false;
                            steps++;
                        } 
                        break;
                    case 1:
                        break;
                    case 2:
                        cmd = new DispenserCommand();
                        Random r = new Random();
                        cmd.BarcodNo = "123" + r.Next(1000, 9999);
                        doneJob = new CloudData();
                        doneJob = AWSSubscriber.RecData;
                        doneJob.Barcode = cmd.BarcodNo;
                        break;
                    case 3:
                        // Move to exchange robot
                        break;
                    case 4:
                        // Send next job cmd
                        cmd.Command = DispenserCMD.NextJob.ToString();
                        mPub.Publish(Topics.T_D_CMD, GF.ConvertObjToJsonString(cmd));
                        break;
                    case 5:
                        break;
                }

                dispenseRoutine.ReportProgress(steps);
                Thread.Sleep(3000);

                if (steps > 0 && steps < 5) steps++;
                else steps = 0;
            }
        }

        private static void dispenseRoutine_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    break;
                case 1:
                    mMainWindow.LB_dispenser.Items.Add($"Get a job type {AWSSubscriber.RecData.nType}, shade {AWSSubscriber.RecData.Shade}, moving to block");
                    break;
                case 2:
                    mMainWindow.LB_dispenser.Items.Add("Barcode: " + cmd.BarcodNo + " is attached to this job");
                    break;
                case 3:
                    mMainWindow.LB_dispenser.Items.Add("Moving to exchange robot");
                    break;
                case 4:
                    mMainWindow.LB_dispenser.Items.Add("Send NextJob Cmd");
                    break;
                case 5:
                    mMainWindow.LB_doneJobs.Items.Add(GF.ConvertObjToJsonString(doneJob));
                    mMainWindow.LB_dispenser.Items.Clear();
                    break;
            }
        }

        private static void dispenseRoutine_WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

    }
}
