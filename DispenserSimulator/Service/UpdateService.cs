﻿using AWSIoTTester.Communication;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static DispenserSimulator.MainWindow;

namespace AWSIoTTester.Service
{
    class UpdateService
    {
        private static BackgroundWorker updateRoutine = new BackgroundWorker();            //Camera preview

        public static void SetupUpdateService()
        {
            updateRoutine.DoWork += new DoWorkEventHandler(updateRoutine_doWork);
            updateRoutine.ProgressChanged += new ProgressChangedEventHandler(updateRoutine_ProgressChanged);
            updateRoutine.RunWorkerCompleted += new RunWorkerCompletedEventHandler(updateRoutine_WorkerCompleted);
            updateRoutine.WorkerReportsProgress = true;
            updateRoutine.WorkerSupportsCancellation = true;
        }
        public static void StartUpdateService()
        {
            if(!updateRoutine.IsBusy)
                updateRoutine.RunWorkerAsync();
        }

        public static void StopUpdateService()
        {
            if (updateRoutine.IsBusy)
                updateRoutine.CancelAsync();
        }

        private static void updateRoutine_doWork(object sender, DoWorkEventArgs e)
        {
            while (!updateRoutine.CancellationPending)
            {
                updateRoutine.ReportProgress(0);
                Thread.Sleep(100);
            }
        }

        private static void updateRoutine_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private static void updateRoutine_WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

    }
}
