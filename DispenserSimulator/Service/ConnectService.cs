﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static DispenserSimulator.MainWindow;
using static AWSIoTTester.Communication.AWSPublisher;
using static AWSIoTTester.Communication.AWSSubscriber;
using static AWSIoTTester.Main.Topics;
using AWSIoTTester.Main;

namespace AWSIoTTester.Service
{
    class ConnectService
    {
        private static BackgroundWorker connectRoutine = new BackgroundWorker();            //Camera preview

        public static void SetupConnectService()
        {
            connectRoutine.DoWork += new DoWorkEventHandler(connectRoutine_doWork);
            connectRoutine.ProgressChanged += new ProgressChangedEventHandler(connectRoutine_ProgressChanged);
            connectRoutine.RunWorkerCompleted += new RunWorkerCompletedEventHandler(connectRoutine_WorkerCompleted);
            connectRoutine.WorkerReportsProgress = true;
            connectRoutine.WorkerSupportsCancellation = true;
        }
        public static void StartConnectService()
        {
            if (!connectRoutine.IsBusy)
                connectRoutine.RunWorkerAsync();
        }

        private static void connectRoutine_doWork(object sender, DoWorkEventArgs e)
        {
            connectRoutine.ReportProgress(0);
            Console.WriteLine("Connecting to publisher thing...");
            mPub.Connect();

            connectRoutine.ReportProgress(1);
            Console.WriteLine("Connecting to subscriber thing...");
            mSub.Connect();

            connectRoutine.ReportProgress(2);
            Console.WriteLine("Subscribing to topic...");
            mSub.Subscribe(Topics.T_C_JOBINFO);
        }

        private static void connectRoutine_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    mMainWindow.lbl_status.Content = "Connecting to CloudSimulator thing...";
                    break;
                case 1:
                    mMainWindow.lbl_status.Content = "Connecting to Dispenser thing..."; 
                    break;
                case 2:
                    mMainWindow.lbl_status.Content = $"Subscribing to topic \"{Topics.T_C_JOBINFO}\""; 
                    break;
            }
        }

        private static void connectRoutine_WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (mPub.IsConnected)
            {
                Console.WriteLine("CloudSimulator as publisher connected to cloud");
                if (mSub.IsConnected)
                {
                    Console.WriteLine("Subscribed to Dispenser on cloud");
                }
            }
        }
    }
}
