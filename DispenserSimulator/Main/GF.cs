﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWSIoTTester.Main
{
    class GF
    {
        public static T ConvertJsonStringToObj<T>(string jsonStr)
        {
            var data = JsonConvert.DeserializeObject<T>(jsonStr);
            return data;
        }

        public static string ConvertObjToJsonString<T>(T _data)
        {
            return JsonConvert.SerializeObject(_data);
        }

        
    }
}
