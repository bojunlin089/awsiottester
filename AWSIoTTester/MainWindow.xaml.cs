﻿using AWSIoTTester.Communication;
using AWSIoTTester.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using static AWSIoTTester.Communication.AWSPublisher;
using static AWSIoTTester.Communication.AWSSubscriber;
using uPLibrary.Networking.M2Mqtt.Messages;
using AWSIoTTester.Service;

namespace AWSIoTTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isMaxmized;
        public static MainWindow mMainWindow = null;

        private const int DEF_WIN_WID = 1000;
        private const int DEF_WIN_HEI = 600;

        public static QueueManager mQManager = new QueueManager();

        public MainWindow()
        {
            mMainWindow = this;
            isMaxmized = false;

            UpdateService.SetupUpdateService();
            ConnectService.SetupConnectService();

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Btn_maxmize_Click(null, null);

            UpdateService.StartUpdateService();
            ConnectService.StartConnectService();
        }

        #region GUI
        private void Btn_maxmize_Click(object sender, RoutedEventArgs e)
        {
            if (!isMaxmized)
            {
                WindowState = WindowState.Maximized;
                this.MainPageArea.Width = SystemParameters.PrimaryScreenWidth;
                this.MainPageArea.Height = SystemParameters.PrimaryScreenHeight;
            }
            else
            {
                WindowState = WindowState.Normal;
                this.MainPageArea.Width = DEF_WIN_WID;
                this.MainPageArea.Height = DEF_WIN_HEI;
            }
            isMaxmized = !isMaxmized;
        }

        private void Btn_minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Btn_close_Click(object sender, RoutedEventArgs e)
        {
            UpdateService.StopUpdateService();
            mQManager.StopQueueManageRoutine();
            try
            {
                mPub.Disonnect();
                mSub.Disconnect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            Close();
            Application.Current.Shutdown();
        }

        private void TitleBar_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }
        #endregion GUI

        private void Btn_genJobs_Click(object sender, RoutedEventArgs e)
        {
            mQManager.GenerateJobs(Convert.ToInt32(TB_totalJobs.Text));
        }
    }
}
