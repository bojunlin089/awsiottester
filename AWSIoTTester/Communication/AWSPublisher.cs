﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;

namespace AWSIoTTester.Communication
{
    /// <summary>
    /// This is the CloudSimulator
    /// </summary>
    class AWSPublisher
    {
        public static AWSPublisher mPub = new AWSPublisher();

        public bool IsConnected; 
        private MqttClient client;
        public AWSPublisher()
        {
            IsConnected = false;
            string iotEndpoint = "alv6svcx5ozmw-ats.iot.us-east-1.amazonaws.com";
            int brokerPort = 8883;

            var caCert = X509Certificate.CreateFromCertFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificates\\CloudSimulator\\AmazonRootCA1.crt"));
            var clientCert = new X509Certificate2(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificates\\CloudSimulator\\certificate.cert.pfx"), "12345678");

            client = new MqttClient(iotEndpoint, brokerPort, true, caCert, clientCert, MqttSslProtocols.TLSv1_2);
        }

        public void Connect()
        {
            Console.WriteLine("AWS IoT Dotnet message publisher starting..");
            string clientId = Guid.NewGuid().ToString();
            client.Connect(clientId);
            Console.WriteLine($"Connected to AWS IoT with client id: {clientId}.");
            IsConnected = true;
        }

        public void Disonnect()
        {
            client.Disconnect();
        }

        public void Publish(string topic, string msg)
        {
            client.Publish(topic, Encoding.UTF8.GetBytes($"{msg}"));
        }
    }
}
