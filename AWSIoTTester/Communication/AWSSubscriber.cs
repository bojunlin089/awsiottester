﻿using AWSIoTTester.Main;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using static AWSIoTTester.MainWindow;

namespace AWSIoTTester.Communication
{    /// <summary>
     /// This subscribing to Dispenser
     /// </summary>
    class AWSSubscriber
    {
        public static AWSSubscriber mSub = new AWSSubscriber();

        public static DispenserCommand RecData;
        public bool IsConnected;

        public MqttClient client;
        public AWSSubscriber()
        {
            RecData = new DispenserCommand();
            IsConnected = false;
            string iotEndpoint = "alv6svcx5ozmw-ats.iot.us-east-1.amazonaws.com";
            int brokerPort = 8883;

            var caCert = X509Certificate.CreateFromCertFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificates\\Dispenser\\AmazonRootCA1.crt"));
            var clientCert = new X509Certificate2(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificates\\Dispenser\\certificate.cert.pfx"), "12345678");

            client = new MqttClient(iotEndpoint, brokerPort, true, caCert, clientCert, MqttSslProtocols.TLSv1_2);

            client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            client.MqttMsgSubscribed += Client_MqttMsgSubscribed;
        }

        public void Connect()
        {
            Console.WriteLine("AWS IoT Dotnet message consumer starting..");
            string clientId = Guid.NewGuid().ToString();
            client.Connect(clientId);
            Console.WriteLine($"Connected to AWS IoT with client ID: {clientId}");
            IsConnected = true;
        }

        public void Disconnect()
        {
            client.Disconnect();
        }

        public void Subscribe(string topic)
        {
            client.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
        }

        // Client call back
        private static void Client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {
            Console.WriteLine($"Successfully subscribed to the AWS IoT topic.");
        }

        private static void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            RecData = GF.ConvertJsonStringToObj<DispenserCommand>(Encoding.UTF8.GetString(e.Message));

            Console.WriteLine(RecData.Command);
            //Application.Current.Dispatcher.Invoke(new Action(() =>
            //{
            //    if (mMainWindow.IsLoaded)
            //        mMainWindow.lbl_dispenserCmd.Content = RecData.Command;
            //}));
        }
    }
}
