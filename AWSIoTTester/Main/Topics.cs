﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWSIoTTester.Main
{
    class Topics
    {
        // For publisher (CloudSimulator)
        public const string T_C_JOBINFO = "CloudSimulator/JobInfo";

        // For subscriber (dispenser)
        public const string T_D_CMD = "Dispenser/Command";

        // Commands
        public enum DispenserCMD
        {
            NextJob
        }

        //public enum CloudStatus
        //{
        //    Idle,
        //    JobReady
        //}
    }
}
