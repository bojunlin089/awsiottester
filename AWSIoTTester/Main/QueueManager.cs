﻿using AWSIoTTester.Communication;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using static AWSIoTTester.Communication.AWSPublisher;
using static AWSIoTTester.Communication.AWSSubscriber;
using static AWSIoTTester.Main.Topics;
using static AWSIoTTester.MainWindow;

namespace AWSIoTTester.Main
{
    public class QueueManager
    {

        public Stack<CloudData> CloudDataQueue;
        public Stack<CloudData> DoneJobQueue;
        private static BackgroundWorker queueManageRoutine = new BackgroundWorker();
        private const int UPDATE_INTERVAL = 3; //s

        public QueueManager()
        {
            CloudDataQueue = new Stack<CloudData>();
            DoneJobQueue = new Stack<CloudData>();

            queueManageRoutine.DoWork += new DoWorkEventHandler(queueManageRoutine_doWork);
            queueManageRoutine.ProgressChanged += new ProgressChangedEventHandler(queueManageRoutine_ProgressChanged);
            queueManageRoutine.RunWorkerCompleted += new RunWorkerCompletedEventHandler(queueManageRoutine_WorkerCompleted);
            queueManageRoutine.WorkerReportsProgress = true;
            queueManageRoutine.WorkerSupportsCancellation = true;
        }

        public void StartQueueManageRoutine()
        {
            if(!queueManageRoutine.IsBusy) queueManageRoutine.RunWorkerAsync();

            mMainWindow.lbl_status.Content = $"Publisher is ready, generate jobs to publish";
        }

        public void StopQueueManageRoutine()
        {
            if (queueManageRoutine.IsBusy) queueManageRoutine.CancelAsync();
        }

        private void queueManageRoutine_doWork(object sender, DoWorkEventArgs e)
        {
            while (!queueManageRoutine.CancellationPending)
            {
                if (CloudDataQueue.Count > 0)
                {
                    mPub.Publish(Topics.T_C_JOBINFO, GF.ConvertObjToJsonString(CloudDataQueue.Peek()));
                    queueManageRoutine.ReportProgress(0);

                    if (RecData.Command == DispenserCMD.NextJob.ToString())
                    {
                        queueManageRoutine.ReportProgress(1);
                    }
                }
                else
                {
                    queueManageRoutine.ReportProgress(0);
                }

                Thread.Sleep(UPDATE_INTERVAL * 1000);
            }
        }

        private void queueManageRoutine_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    if (CloudDataQueue.Count > 0)
                    {
                        mMainWindow.lbl_jobInfo.Content = CloudDataQueue.Peek().JobID;
                        mMainWindow.lbl_status.Content = $"Now publishing Job info to \"{Topics.T_C_JOBINFO}\"";
                    }
                    else mMainWindow.lbl_jobInfo.Content = "NoJob";
                    break;
                case 1:
                    NextJob();
                    break;
            }
        }

        private void queueManageRoutine_WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        public void GenerateJobs(int num)
        {
            CloudDataQueue.Clear();
            mMainWindow.LB_jobInQueue.Items.Clear();

            uint total = 0;
            for (int i = 0; i < num; i++)
            {
                total++;
                Random r = new Random();
                CloudData data = new CloudData() { TotalJobs = (uint)num, JobID = "1122" + (1000+total), ToothNumber = r.Next(10, 50), 
                    nType = (uint)r.Next(1, 3), Shade = ((BlockShade)r.Next(1, 16)).ToString(), TimeStamp = DateTime.Now.ToString() };

                CloudDataQueue.Push(data);
                mMainWindow.LB_jobInQueue.Items.Add(GF.ConvertObjToJsonString(data));
                Thread.Sleep(10);
            }
        }

        public void NextJob()
        {
            // Receive information from dispenser
            CloudData temp = CloudDataQueue.Peek();
            temp.Barcode = RecData.BarcodNo;

            DoneJobQueue.Push(temp);
            mMainWindow.LB_jobDone.Items.Add(GF.ConvertObjToJsonString(temp));

            if (CloudDataQueue.Count > 0)
            {
                // Remove Job
                CloudDataQueue.Pop();
                foreach (CloudData data in CloudDataQueue)
                {
                    data.TotalJobs--;
                }
                mMainWindow.LB_jobInQueue.Items.RemoveAt(CloudDataQueue.Count);
            }

            // Reset received data
            RecData = new DispenserCommand();
        }
    }
}
