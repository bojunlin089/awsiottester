﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWSIoTTester.Main
{
    public class CloudData
    {
        // From Cloud
        public uint TotalJobs { get; set; }
        public string JobID { get; set; }
        public int? ToothNumber { get; set; }
        public uint nType { get; set; }
        public string Shade { get; set; }
        public string TimeStamp { get; set; }

        // From dispenser
        public string Barcode { get; set; }
        public string NCFileName { get; set; }

        public CloudData()
        {
            TotalJobs = 0;
            JobID = "";
            ToothNumber = 0;
            nType = 0;
            Shade = "";
            TimeStamp = "";
            Barcode = "";
            NCFileName = "";
        }
    }

    public class DispenserCommand
    {
        // From dispenser
        public string Command { get; set; }
        public string JobID { get; set; }
        public string BarcodNo { get; set; }

        public DispenserCommand()
        {
            Command = "";
            JobID = "";
            BarcodNo = "";
        }
    }

    public enum BlockShade
    {
        A1 = 1,
        A2 = 2,
        A3 = 3,
        A3_5 = 4,
        A4 = 5,
        B1 = 6,
        B2 = 7,
        B3 = 8,
        B4 = 9,
        C1 = 10,
        C2 = 11,
        C3 = 12,
        C4 = 13,
        D2 = 14,
        D3 = 15,
        D4 = 16,
    }
}
